## Beautifulhugo Customized

Dies ist eine Anpassung des Beautiful Hugo Themes und mein Versuch Erweiterungen und Fixes in einem Theme zusammenzuführen.

Ehre gebührt [halogenica](https://github.com/halogenica/beautifulhugo) der das Theme zu Hugo geportet hat und [daattali](https://github.com/daattali/beautiful-jekyll) für das Erstellen des original Jekyll Themes.

## Startseiten Inhalt
`beautifulhugo-customized` unterstützt Inhalte auf der Startseite. Editiere `/content/_index.md` um anzupassen was hier erscheint oder lösche die Datei um nichts darzustellen. 
