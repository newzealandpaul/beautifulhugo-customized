---
title: Über mich
subtitle: Haben Sie meine Truhe gesehen?
comments: false
---

Hallo mein Name ist Zweiblum.

Ich komme aus dem Achatenen Reich, auf dem den Gegengewicht Kontinent, und bin Spezialist für Risikobewertung einer Versicherung in Bes Pelargic.

 Ich bin der erste Tourist der Scheibenwelt und schrieb den Bestseller "Wie ich meine Ferien verbrachte".

Ich besuchte Ankh-Morpork, wo ich den Zaubberer Rincewind kennen lernte und ihn als Guide anheuerte. Begleitet wurde ich von Truhe, eine hölzerne Reisetruhe aus intelligentem Birnbaumholz mit hübschen Messingbeschlägen, unzähligen kleinen Beinchen und einem recht großen Appetit.

### Meine Reisen

Wenn du mehr über meine Reisen wissen möchtest, empfehle ich dir "_Die Farben der Magie_" zu [lesen](https://de.wikipedia.org/wiki/Die_Farben_der_Magie) oder [anzusehen](https://www.imdb.com/title/tt1079959/).
