---
title: Shortcodes
date: 2020-01-16
subtitle: "Zusätzliche shortcodes in beautifulhugo-customized"
tags: ["example", "beautifulhugo-customized", "shortcodes", "ipsum"]
---
Die ist eine kurze Demo der Shortcodes `details` und `columns` von Beautifulhugo Customized.
<!--more-->

### Details

{{< details "Dies ist der Titel von details (Klicken zum erweitern)" >}}
Dies ist der Inhalt, versteckt bis zum Klick.

Zombie ipsum reversus ab viral inferno, nam rick grimes malum cerebro. De carne lumbering animata corpora quaeritis. Summus brains sit​​, morbo vel maleficia? De apocalypsi gorger omero undead survivor dictum mauris. Hi mindless mortuis soulless creaturas, imo evil stalking monstra adventus resi dentevil vultus comedat cerebella viventium.

Qui animated corpse, cricket bat max brucks terribilem incessu zomby. The voodoo sacerdos flesh eater, suscitat mortuos comedere carnem virus. Zonbi tattered for solum oculi eorum defunctis go lum cerebro. Nescio brains an Undead zombies. Sicut malus putrid voodoo horror. Nigh tofth eliv ingdead.

[Zombieipsum](http://www.zombieipsum.com/)
{{< /details >}}

### Split

{{< columns >}}
Dies ist Spalte 1.

Cat ipsum dolor sit amet, cornish rex devonshire rex or cheetah cougar. American shorthair kitten turkish angora cornish rex yet leopard leopard. Mouser devonshire rex scottish fold for siberian for ocicat. Donskoy cougar himalayan kitten. Grimalkin egyptian mau but persian but devonshire rex ragdoll for manx. British shorthair devonshire rex devonshire rex for bengal maine coon for russian blue for russian blue. Balinese cougar. Persian sphynx yet tiger. Bombay maine coon for kitty for mouser. Kitty. Malkin. Savannah. Grimalkin cougar.

Ragdoll siamese manx and savannah. Devonshire rex panther cornish rex but american shorthair leopard. Bengal leopard for havana brown so bobcat persian panther. Cheetah kitten yet bobcat. Sphynx burmese panther devonshire rex american bobtail. Bobcat. Persian british shorthair cornish rex. Turkish angora maine coon, for american bobtail puma so bobcat lynx, so savannah. Siberian. Puma himalayan so grimalkin so ocelot lion. Turkish angora burmese so mouser, so sphynx. Abyssinian grimalkin.

Mouser tiger havana brown. Norwegian forest maine coon leopard. Ocicat. Bengal tiger balinese so thai but bobcat for singapura. Tiger tomcat panther and kitty. Persian ragdoll cougar. Bombay kitten lynx so panther siamese, so egyptian mau, sphynx. Thai tom grimalkin sphynx leopard for american shorthair. Siamese siamese yet donskoy or abyssinian egyptian mau bengal scottish fold. Kitten persian devonshire rex or tiger.

[Catipsum](http://www.catipsum.com)
{{< column >}}
Dies ist Spalte 2.

Bacon ipsum dolor amet doner buffalo bacon kielbasa, tri-tip ribeye ball tip biltong rump meatball flank cow pork pork loin. Landjaeger corned beef flank spare ribs, beef jerky tri-tip leberkas picanha doner pork belly kevin bacon shank venison. Tenderloin shank ground round, ham spare ribs beef chuck tongue pork loin short loin salami flank. Pig meatball rump, biltong venison turkey tenderloin pork bacon doner chicken ground round chuck t-bone. Tenderloin kevin pancetta prosciutto capicola pastrami porchetta filet mignon ham hock turducken flank.

Meatball leberkas shoulder buffalo kevin ham bacon meatloaf ribeye. Salami strip steak capicola hamburger meatloaf tri-tip t-bone ball tip sausage ham hock prosciutto pork boudin rump pastrami. Sausage picanha rump filet mignon meatloaf venison pork chop chislic biltong jowl hamburger. Pancetta sirloin hamburger, spare ribs tail prosciutto salami kielbasa bresaola boudin fatback ham hock. Fatback alcatra sirloin swine, pork kielbasa picanha biltong meatloaf ham chislic pastrami boudin.

Beef ribs burgdoggen drumstick, swine doner shankle tail flank pork brisket chislic capicola tenderloin bacon. T-bone pork loin shank swine alcatra. Flank meatloaf shankle, rump ham hock pork loin turducken short ribs. Jerky drumstick filet mignon, picanha strip steak tri-tip pastrami andouille fatback sausage frankfurter doner. Tri-tip beef ribs drumstick venison bresaola. Shankle capicola pork ribeye.

[baconipsum](https://baconipsum.com)
{{< endcolumns >}}
