---
title: "Terry Pratchett"
date: 2015-12-03
subtitle: "GNU Terry Pratchett"
image: "https://upload.wikimedia.org/wikipedia/commons/7/7a/10.12.12TerryPratchettByLuigiNovi1.jpg"
tags: ["example", "Terry Pratchett"]
---

Auszug aus der [Wikipedia](https://de.wikipedia.org/wiki/Terry_Pratchett)

Sir Terence David John Pratchett, OBE (* 28. April 1948 in Beaconsfield, Buckinghamshire; † 12. März 2015 in Broad Chalke, Wiltshire) war ein britischer Fantasy-Schriftsteller. Seine bekanntesten Werke sind seine Scheibenwelt-Romane, die in 37 Sprachen übersetzt wurden. Weltweit wurden rund 85 Millionen seiner Bücher verkauft.

Terry Pratchett veröffentlichte sein erstes Werk mit 13 Jahren. Es war die Kurzgeschichte _The Hades Business_, die erst in der Schülerzeitung und später im Science Fantasy Magazine veröffentlicht wurde.
<!--more-->

Neben Pratchetts sehr eigenem Schreibstil, insbesondere durch seinen in der Fantasyliteratur genreunüblichen Humor bis hin zur Persiflage, zeichnen sich einige seiner Bücher durch die zum Teil überbordende Verwendung von Fußnoten aus. Die niederländische Übersetzung von Good Omens, das Pratchett in Co-Autorenschaft mit Neil Gaiman schrieb, beginnt mit einem ironischen Vorwort des Übersetzers, in dem er versichert, keine zusätzlichen Fußnoten eingebaut zu haben, um eventuelle Unklarheiten zu beseitigen – ergänzt um Fußnoten zur Klärung von Omen und Crowley.

Im Dezember 2007 gab Pratchett bekannt, dass bei ihm eine sehr seltene, früh beginnende Form der Alzheimer-Krankheit diagnostiziert worden war. Über die Krankheit sprach Pratchett am 13. März 2008 in einem Interview in der BBC-Radio-Sendung Today Programme. Nach eigener Aussage hat er seinen Führerschein abgegeben und Schwierigkeiten beim Maschinenschreiben, weshalb er seine Texte seitdem mit Hilfe einer Spracherkennungssoftware verfasste. Pratchett spendete eine Million Dollar an den Alzheimer Research Trust und kritisierte, dass für die Erforschung von Demenzerkrankungen zu wenige Mittel bereitgestellt werden. "_Ich würde den Hintern eines toten Maulwurfs essen, wenn mir damit geholfen wäre_", sagte er.

![Terry Pratchett](https://upload.wikimedia.org/wikipedia/commons/7/7a/10.12.12TerryPratchettByLuigiNovi1.jpg)
