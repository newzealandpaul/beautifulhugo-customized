---
title: Math Beispiele
subtitle: Nutze KaTeX
date: 2017-03-05
tags: ["example", "math"]
---

KaTeX kann benutzt werden um komplexe mathematische Formeln serverseitig darzustellen.

$$
\phi = \frac{(1+\sqrt{5})}{2} = 1.6180339887\cdots
$$

Zusätzliche Details können auf [GitHub](https://github.com/Khan/KaTeX) oder dem [Wiki](http://tiddlywiki.com/plugins/tiddlywiki/katex/) gefunden werden.
<!--more-->

### Beispiel 1

Wenn der Text zwischenden `$$` einen Zeilenumbruch enthält wir es im Display Mode dargestellt:
```plain
$$
f(x) = \int_{-\infty}^\infty\hat f(\xi)\,e^{2 \pi i \xi x}\,d\xi
$$
```
$$
f(x) = \int_{-\infty}^\infty\hat f(\xi)\,e^{2 \pi i \xi x}\,d\xi
$$


### Beispiel 2
```plain
$$
\frac{1}{\Bigl(\sqrt{\phi \sqrt{5}}-\phi\Bigr) e^{\frac25 \pi}} = 1+\frac{e^{-2\pi}} {1+\frac{e^{-4\pi}} {1+\frac{e^{-6\pi}} {1+\frac{e^{-8\pi}} {1+\cdots} } } }
$$
```
​​$$
\frac{1}{\Bigl(\sqrt{\phi \sqrt{5}}-\phi\Bigr) e^{\frac25 \pi}} = 1+\frac{e^{-2\pi}} {1+\frac{e^{-4\pi}} {1+\frac{e^{-6\pi}} {1+\frac{e^{-8\pi}} {1+\cdots} } } }
$$
​​ 

### Beispiel 3
```plain
$$
1 +  \frac{q^2}{(1-q)}+\frac{q^6}{(1-q)(1-q^2)}+\cdots = \prod_{j=0}^{\infty}\frac{1}{(1-q^{5j+2})(1-q^{5j+3})}, \quad\quad \text{for }\lvert q\rvert<1.
$$
```
$$
1 +  \frac{q^2}{(1-q)}+\frac{q^6}{(1-q)(1-q^2)}+\cdots = \prod_{j=0}^{\infty}\frac{1}{(1-q^{5j+2})(1-q^{5j+3})}, \quad\quad \text{for }\lvert q\rvert<1.
$$
